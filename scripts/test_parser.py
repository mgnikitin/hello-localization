#!/bin/env python

import unittest
from parser import *

class TestParser(unittest.TestCase):

    def test_literal(self):
        self.assertEqual(parse(r'L"wide-character\" text1\"  \" text2\" string"'), 'wide-character\\" text1\\"  \\" text2\\" string')


if __name__ == '__main__':
    unittest.main()
