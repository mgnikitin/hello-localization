#!/bin/env python
# -*- coding: utf-8 -*-

import getopt, sys, os
import fnmatch
import re
#from dicttoxml import dicttoxml
import xmltodict
from xml.sax.saxutils import (escape, unescape, quoteattr)

__regex = r'(?P<prefix>(?:\b[Lu])?)(?:"(?P<dbl>[^"\\\n<>]*(?:\\.[^"\\]*)*)"|\'(?P<sngl>[^\'\\n<>]*(?:\\.[^\'\\]*)*)\')|R"\((?P<raw>.*?)\)"'
__regex_cyr = ur'[А-я]+'
__p = re.compile(__regex)

__verbose=False
__dict = {}

class Context:
    filename=''

__ctx = Context()

#-------------------------------
def parse_file(filename):

    __ctx.filename = filename

    with open(filename, 'r') as file:
        filetext = file.read()
        parse(filetext)

#-------------------------------
def parse_dir(path):

    for root, dirs, files in os.walk(path, followlinks=False):
        p = root.split(os.sep)
        for file in fnmatch.filter(files, "*.cpp"):
            fullpath=os.path.join(root, file)
            if __verbose:
                print 'parsing %s' % fullpath
            parse_file(fullpath)

#-------------------------------
def parse(text):

    try:
        for x in __p.finditer(text):
            if x.group("dbl"):
                dict_append(x.group("dbl").decode('utf8'))
            elif x.group("sngl"):
                dict_append(x.group("sngl").decode('utf8'))
            elif x.group("raw"):
                dict_append(x.group("raw").decode('utf8'))
    except TypeError:
        sys.exit('Parsing error\n')
    except IndexError:
        sys.exit('Parsing error\n')
    except UnicodeDecodeError:
        sys.stderr.write('Encoding error, suppose file encode differ utd-8, skip\n')

#-------------------------------
def dict_append(key):
    if len(key) > 1024:
        sys.stderr.write('Suppose something going wrong in file "'+__ctx.filename+'", skip\n')
        return
    if re.search(__regex_cyr, key):
        if not key in __dict:
            __dict[key] = [{'test': key}]

#-------------------------------
def load_dict(dictname):

    d = {}
    with open(dictname, 'r') as file:
        filetext = file.read()
        d = xmltodict.parse(filetext)

    for item in d.get('root').get('literal'):
        key = None
        list=[]
        for k in item:
            if '@key' == k:
                key=item['@key']
            else:
                list.append({k:unescape(item[k], {"&apos;": "'", "&quot;": '"'})})
        __dict[key]=list

#-------------------------------
def dict2xml__(d, root_node=None):
    wrap          = False if None == root_node else True
    xml           = ''
    children      = []

    if isinstance(d, dict):
        for key, value in dict.items(d):
            if isinstance(value, dict):
                children.append(dict2xml__(value, escape(key.encode('utf8'))))
            elif isinstance(value, list):
                children.append(dict2xml__(value, escape(key.encode('utf8'))))
            else:
                children.append('<' + escape(key.encode('utf8')) + '>' + escape(value.encode('utf8')) + '</' + escape(key.encode('utf8')) + '>')
    elif isinstance(d, list):
        for value in d:
            children.append(dict2xml__(value, root_node))
    else:
        return d

    end_tag = '>' if 0 < len(children) else '/>'

    if wrap:
        xml = '<' + escape(root_node) + end_tag

    if 0 < len(children):
        for child in children:
            xml = xml + child

        if wrap:
            xml = xml + '</' + root_node + '>'

    return xml

#-------------------------------
def dict2xml(d, root_node=None):
    root          = 'root' if None == root_node else escape(root_node)
    xml           = ''
    children      = []

    if isinstance(d, dict):
        for key, value in dict.items(d):
            if isinstance(value, list):
                children.append('<literal key=' + quoteattr(escape(key.encode('utf8'))) + '>' + dict2xml__(value, None)+ '</literal>')
            else:
                children.append('<literal key=' + quoteattr(escape(key.encode('utf8'))) + '>' + escape(value.encode('utf8')) + '</literal>')

    end_tag = '>' if 0 < len(children) else '/>'
    xml = '<' + root + end_tag

    if 0 < len(children):
        for child in children:
            xml = xml + child

        xml = xml + '</' + root + '>'

    return xml

#-------------------------------
def usage():

    print ' -------------------------------------------------------------------------'
    print ' Usage:'
    print '     parser.py --file=test.cpp'
    print ' '
    print ' Available options:'
    print ' -f, --file <filename> - parse <filename>'
    print ' --dir <path> - use directory for parse'
    print ' --dict <filename> - use dictionary <filename>'
    print ' -v, --verbose - print debug info'
    print ' -------------------------------------------------------------------------'
    sys.exit(' ')

#-------------------------------
def repr_dict(d):
    return '{%s}' % ',\n'.join("'%s': '%s'" % pair for pair in d.iteritems())

#-------------------------------
def main():

    global __verbose

    try:
        opts, args = getopt.getopt(sys.argv[1:],"hf:v",
                       ["help","file=","dir=","dict=","verbose"])
    except getopt.GetoptError:
        usage()

    filename=None
    dictname=None
    path=None
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
        if o in ("-f", "--file"):
            filename = str(a)
        if o in ("--dir"):
            path = str(a)
        if o in ("--dict"):
            dictname = str(a)
        if o in ("-v", "--verbose"):
             __verbose=True

    if dictname:
        load_dict(dictname)

    if path:
        parse_dir(path)
    elif filename:
        parse_file(filename)

    xml = dict2xml(__dict)
    if dictname and (path or filename):
        with open(dictname, 'w') as file:
            file.write(xml)
    else:
        print xml


#-------------------------------
if __name__ == "__main__":

    main()
