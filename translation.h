#ifndef TRANSLATION_H
#define TRANSLATION_H

#include <utility>
#include <string>
#include <tuple>
#include "const_crc32.h"

namespace {

template<unsigned T>
struct always_false : std::false_type {};

constexpr size_t length(const char* str) {

    return *str ? 1 + length(str + 1) : 0;
}

constexpr size_t const_hash(char const *input) {
    return const_crc32(input, length(input));
}

template<size_t T>
struct const_transl__ {
    const char *operator()() { static_assert(always_false<T>::value, "error: translation is not implemented");  return ""; }
};

template<>
struct const_transl__<const_hash("world")> {
    const char *operator()() { return "мир"; }
};

template<>
struct const_transl__<const_hash("hello")> {
    const char *operator()() { return "привет"; }
};

}

#define _(arg) const_transl__<const_hash(arg)>()()


#endif // TRANSLATION_H
